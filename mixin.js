// 根目录的static下创建js文件夹并创建mixin.js文件
import {
	track,
	close,
	ping
} from '@/api/app.js';
const mixin = {
	methods: {
		track() {
			let routes = getCurrentPages()
			let curRoute = routes[routes.length - 1].route //获取当前页面路由
			let curParam = routes[routes.length - 1].options //获取路由参数
			track({
					"route": curRoute,
					"param": curParam,
				}).then(res => {})
				.catch(err => {});
		},
		close() {
			close().then(res => {})
				.catch(err => {});
		},
		ping() {
			// console.log("心跳")
			ping().then(res => {})
				.catch(err => {});
		}
	}
}
export default mixin
