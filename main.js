import Vue from 'vue'
import App from './App'
import store from './store'
import {
	http
} from '@/api/service.js' // 全局挂载引入，配置相关在该index.js文件里修改

Vue.prototype.$http = http
Vue.prototype.$tv = store

if (process.env.NODE_ENV === 'development') {
	Vue.prototype.host = 'http://119.97.159.200';
} else {
	Vue.prototype.host = 'http://119.97.159.200';
}
http.config.baseURL = Vue.prototype.host

// 全局mixin
import mixin from './mixin'
Vue.mixin(mixin)

Vue.config.productionTip = false
App.mpType = 'app'

const app = new Vue({
	store,
	...App
})
app.$mount()
