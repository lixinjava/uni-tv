import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		smartcard: null,
		currentPage: null,
		currentZone: null,
		allPages: []
	},
	mutations: {
		setSmartCard(state, smartcard) {
			state.smartcard = smartcard;
		},
		pushPage(state, page) {
			state.allPages[page.id] = page;
		},
		switchZone(state, zone) {
			state.currentZone = zone
		},
		switchPage(state, page) {
			state.currentPage = page;
		},
		showPage(state, pageId) {
			var page = state.allPages[pageId];
			if (page) {
				page.showPage();
			}
		}
	},
	actions: {

	}
})

export default store
