# Uniapp-TV

#### 介绍
使用uni-app 实现TV遥控器焦点控制


![截图](https://images.gitee.com/uploads/images/2021/0307/161840_5b2b4b56_1161.png "snapshot.png")

**按键监听实现原理：**

参照：https://ext.dcloud.net.cn/plugin?id=2290

核心按键监听代码：

```
<template>
<view @click="whole.onClick" id="onClick"></view>
</template>
<script module="whole" lang="renderjs">
	let code;
	let KeyName = {
		19: 'KeyUp',
		38: 'KeyUp', //Keyboard
		20: 'KeyDown',
		40: 'KeyDown', //Keyboard
		21: 'KeyLeft',
		37: 'KeyLeft', //Keyboard
		22: 'KeyRight',
		39: 'KeyRight', //Keyboard
		23: 'KeyEnter',
		13: 'KeyEnter', //Keyboard
		4: 'KeyBack',
		18: 'KeyBack', //Keyboard Alt键
		27: 'KeyBack', //Keyboard ESC
		24: 'KeyBack', //Keyboard ESC
		66: 'KeyEnter',
		111: 'KeyBack'
	};
	export default {
		mounted() {
			//全局监听按键输入
			window.document.onkeydown = function(evt) {
				evt = evt || window.event;
				
				var KeyCode = evt.which || evt.keyCode;
				//document.getElementById("onClick").click();
				code = KeyName[KeyCode];
				if(code!='KeyBack'){
					evt.preventDefault();
				}
				if(code!=undefined){
				document.getElementById("onClick").click();
				}
			}

		},
		methods: {
			onClick(event, ownerInstance) {
				// 调用 service 层的方法
				ownerInstance.callMethod('keyCodeClick', code);

			}
		}
	}
</script>
```

### 组件使用说明

**unitv-page** 代表一页，所有焦点控制内容都需要在它内部。

例如：

```
<unitv-page id="indexPage" :show="true" ref="unitvPage" @back="pageBack">
			<view>
				<unitv-zone id="zone1" class="zone1" :autoFous="true" down="zone2"  :item="3" :values="item1" :column="item1.length">
					<unitv-scroll>
						<unitv-item v-for="(a,index) in item1" :item="index" :key="index" class="item" @hover="hoverItem">{{a}}</unitv-item>
					</unitv-scroll>
				</unitv-zone>
			</view>
</unitv-page>
```

参数说明：

| 参数名称 | 是否必须 | 参数说明                |
| -------- | -------- | ----------------------- |
| id       | 是       | 全局唯一ID              |
| show     | 否       | 页面是否显示 默认 false |
| @back    | 否       | 可用于监听返回事件      |



**unitv-zone** 代表一块区域，相当于将所有焦点进行分组，方便控制区域之间切换

例如：

```
<unitv-zone id="zone1" class="zone1" :autoFous="true" down="zone2"  :item="3" :values="item1" :column="item1.length">
					<unitv-scroll>
						<unitv-item v-for="(a,index) in item1" :item="index" :key="index" class="item" @hover="hoverItem">{{a}}</unitv-item>
					</unitv-scroll>
</unitv-zone>
```

参数说明：

| 参数名称 | 是否必须 | 参数说明                                         |
| -------- | -------- | ------------------------------------------------ |
| id       | 是       | 页面内唯一ID                                     |
| autoFous | 否       | 是否是默认选中区域 默认否                        |
| up       | 否       | 区域边界 按"上"键切换到的区域                    |
| down     | 否       | 区域边界 按"下"键切换到的区域                    |
| left     | 否       | 区域边界 按"左"键切换到的区域                    |
| right    | 否       | 区域边界 按"右"键切换到的区域                    |
| item     | 否       | 区域内默认选中的子项 默认0 第一个                |
| row      | 否       | 总行数，区域内总的有多少行 默认1行  支持动态计算 |
| column   | 是       | 总列数，区域内有多少列                           |
| values   | 否       | 区域的子项数组                                   |
| count    | 否       | 区域的子项总数 count，values 应当二选一          |

**unitv-item** 代表一块区域内的一个子项

例如：

```
<unitv-item v-for="(a,index) in item1" :item="index" :key="index" class="item" @hover="hoverItem">{{a}}</unitv-item>
```

参数说明：

| 参数名称    | 是否必须 | 参数说明                                                     |
| ----------- | -------- | ------------------------------------------------------------ |
| item        | 是       | 当前子项下标  按键发生后会动态计算区域内下一个焦点的下标位置，匹配则选中 |
| hoverClass  | 否       | 滑过时的样式                                                 |
| selectClass | 否       | 确认选中时的样式 通常当前区域已经切换到其他区域时 保留当前位置高亮 |
| @hover      | 否       | 滑过时触发事件                                               |
| @click      | 否       | 按下确认键触发事件                                           |

**unitv-scroll** 代表一块区域内子项超过屏幕宽度 当下一焦点不可见时，需要滚动至可见范围

**unitv-video**封装video组件并用于监听按键控制暂停，播放，快进，快退

注意事项：

1、页面返回时需恢复当前页显示，可调用unitv-page的showPage方法。

```
this.$refs.unitvPage.showPage()
```

2、同一个page中支持多个 unitv-page 可加快页面显示速度，但需要自行维护和控制页面显示。

