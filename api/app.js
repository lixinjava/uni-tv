/**
 * 商品相关api
 */
import {
	http
} from '@/api/service.js'

/**
 * 首页数据
 * @param {Object} params - 查询参数  
 */
export const getIndexData = () => {
	return http.get('/api/init/index')
}

/**
 * 公司主页数据
 * @param {Object} params - 查询参数  
 */
export const getMainData = (params) => {
	return http.get('/api/init/main', {
		params
	})
}

/**
 * 广告详情页信息
 * @param {Object} params - 查询参数  
 */
export const getDetailsData = (params) => {
	return http.get('/api/init/details', {
		params
	})
}

/**
 * APP启动
 * @param {Object} params - 是否为第一次启动
 */
export const launch = (params) => {
	return http.get('/api/init/launch', {
		params
	})
}

/**
 * APP关闭
 */
export const close = () => {
	return http.get('/api/init/close')
}

/**
 * 行动轨迹
 * @param {Object} params - 查询params参数
 */
export const track = (params) => {
	return http.get('/api/init/track', {
		params
	})
}

/**
 * 心跳
 * @param {Object} params - 查询params参数
 */
export const ping = () => {
	return http.get('/api/init/ping')
}
